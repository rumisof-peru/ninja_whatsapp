
const express = require('express');
const SessionUseCase = require('../usecases/SessionUseCase');

const router = express.Router();
const sessionUseCase = new SessionUseCase();

router.post('/create', async (req, res) => {
    const { sessionName,socketId } = req.body;
    const result = await sessionUseCase.createSession(sessionName,socketId);

    if (result.success) {
        res.status(200).json({status:"ok",mesaje:result.message});
    } else {
        res.status(500).json({status:"fail",mesaje:result.message});
    }
});

router.post('/send', async (req, res) => {

    const { sessionName , to, content } = req.body;
    const result=await sessionUseCase.sendMessage(sessionName, { to, content });
    if (result.success) {
        res.status(200).json({status:"ok",mesaje:result.message});
    } else {
        res.status(500).json({status:"fail",mesaje:result.message});
    }
});

router.post('/sendImgBase64', async (req, res) => {
    const { sessionName,to, base64, descripcion  } = req.body;
    const result=await sessionUseCase.sendImgBase64(sessionName,{to, base64, descripcion });
    if (result.success) {
        res.status(200).json({status:"ok",mesaje:result.message});
    } else {
        res.status(500).json({status:"fail",mesaje:result.message});
    }
});

router.post('/sendPdfBase64', async (req, res) => {
    const { sessionName,to, base64, descripcion  } = req.body;
    const result=await sessionUseCase.sendImgBase64(sessionName,{to, base64, descripcion });
    if (result.success) {
        res.status(200).json({status:"ok",mesaje:result.message});
    } else {
        res.status(500).json({status:"fail",mesaje:result.message});
    }
});

router.post('/getallcontac', async (req, res) => {
    const { sessionName } = req.body;
    const result=await sessionUseCase.getAllContac(sessionName);
    if (result.success) {
        res.status(200).json({status:"ok",mesaje:result.message});
    } else {
        res.status(500).json({status:"fail",mesaje:result.message});
    }
});

router.post('/sendLocation', async (req, res) => {  
    const { sessionName, to, lat,long } = req.body;
    const result=await sessionUseCase.sendLocation(sessionName, {to, lat, long });
    if (result.success) {
        res.status(200).json({status:"ok",mesaje:result.message});
    } else {
        res.status(500).json({status:"fail",mesaje:result.message});
    }
});

router.post('/sendMenu', async (req, res) => {
    const { sessionName, to, menu } = req.body;
    const result=await sessionUseCase.sendMenu(sessionName, {to, menu });
    if (result.success) {
        res.status(200).json({status:"ok",mesaje:result.message});
    } else {
        res.status(500).json({status:"fail",mesaje:result.message});
    }
});

router.post('/prueba/:sessionName', async (req, res) => {
    const { sessionName } = req.params;
    const result=await sessionUseCase.sendPrueba(sessionName);
    if (result.success) {
        res.status(200).json({status:"ok",mesaje:result.message});
    } else {
        res.status(500).json({status:"fail",mesaje:result.message});
    }
});



module.exports = router;
