
const VenomRepository = require('../repositories/VenomRepository');

class SessionUseCase {
    constructor() {
        this.venomRepository = new VenomRepository();
    }


    async createSession(sessionName,socketId) {
        return await this.venomRepository.createSession(sessionName,socketId);
    }

    async sendMessage(sessionName, {to, content }) {
        return await this.venomRepository.sendMessage(sessionName, { to, content });
    }

    async sendImgBase64(sessionName,{to, base64, descripcion}) {
        return await this.venomRepository.sendImgBase64(sessionName,{to, base64, descripcion});
    }

    async sendPdfBase64(sessionName,{to, base64, descripcion}) {
        return await this.venomRepository.sendPdfBase64(sessionName,{to, base64, descripcion});
    }

    async getAllContac(sessionName) {
        return await this.venomRepository.getAllContac(sessionName);
    }

    async sendLocation(sessionName,{to, lat, long }) {
        return await this.venomRepository.sendLocation(sessionName, {to, lat,long });
    }

    async sendMenu(sessionName,{to, menu}) {
        return await this.venomRepository.sendMenu(sessionName, {to, menu });
    }

    async sendPrueba(sessionName) {
        return await this.venomRepository.sendPrueba(sessionName);
    }

}

module.exports = SessionUseCase;

