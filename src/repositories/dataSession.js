const fs = require('fs');

const dbFilePath = 'data.json';

function loadData() {
  try {
    return JSON.parse(fs.readFileSync(dbFilePath));
  } catch (error) {
    return [];
  }
}

function saveData(data) {
  fs.writeFileSync(dbFilePath, JSON.stringify(data, null, 2));
}

function createOrUpdateItem(item) {
  const data = loadData();
  const existingIndex = data.findIndex(obj => obj.id === item.id);
  if (existingIndex !== -1) {
    data[existingIndex] = item;
  } else {
    data.push(item);
  }
  saveData(data);
}

function findItemById(id) {
  const data = loadData();
  return data.find(item => item.id === id);
}

function updateItem(id, newData) {
  const data = loadData();
  const index = data.findIndex(item => item.id === id);
  if (index !== -1) {
    data[index] = { ...data[index], ...newData };
    saveData(data);
    return true;
  }
  return false;
}

function deleteItem(id) {
  let data = loadData();
  data = data.filter(item => item.id !== id);
  saveData(data);
}

module.exports = {
  createOrUpdateItem,
  findItemById,
  updateItem,
  deleteItem
};
