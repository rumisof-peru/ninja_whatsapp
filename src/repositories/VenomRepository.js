const ioClient = require('socket.io-client');
require('dotenv').config();
const venom = require('venom-bot');
// const { Client, LocalAuth, MessageMedia, List } = require("whatsapp-web.js");
// const { createOrUpdateItem, findItemById, updateItem, deleteItem } = require('./dataSession');

// var qrcode = require('qrcode-terminal');
// const fs = require('fs');
// const mime = require('mime-types');
// const path = require('path');


class VenomRepository {

    constructor() {
        this.sessions = {};
        this.connected = false;
        this.waPages = new Map();
        this.isSssion = 0;
        this.observador;

        if (!global.clients) {
            global.clients = [];
        }



        this.socket = ioClient(process.env.SOCKETTOKEN_URL, {
            transportOptions: {
                polling: {
                    extraHeaders: {
                        'x-token': process.env.SOCKETTOKEN
                    }
                }
            }
        });

        this.socket.on('connect', () => {
            this.connected = true;
            console.log('Conectado al servidor WebSocket', this.connected);
        });

        this.socket.on('disconnect', () => {
            console.log('Desconectado del WebSocket');
            this.connected = false;
        });

        this.socket.on('message', message => {
            console.log('>>>>>>>>>>>>>>>> :', message, "\n");
        });
    }



    async startSessionVenon(sessionName, socketId) {
        console.log(sessionName);
        console.log(socketId);

        try {
            const client = await venom.create(
                { session: sessionName },
                (base64Qrimg, asciiQR, attempts, urlCode) => {
                    let numeroEntero = attempts;
                    //this.socket.emit('privateSendQR', { to: "477931508", intentos: numeroEntero, qr: base64Qrimg });
                },
                (statusSession, session) => {
                    console.log('>>>>>>>>Estado de la sesión: ', statusSession, session);
                },
                (browser, waPage) => {
                    // waPage.screenshot({ path: 'screenshot125.png' });
                    // this.waPages.set(sessionName, waPage);
                }
            );

            console.log(`Sesión ${sessionName} iniciada`);

            //console.log("----> ",JSON.stringify(client));

            client.socketIdM = socketId;

            this.listenMessages(client);


            // await this.captureScreenshot("mifotocalato.png");
            return client;
        } catch (error) {
            console.error(`----->Error al iniciar sesión ${sessionName}:`, error);
            //throw error;
        }
    }

    listenMessages = (client) => {
        // Listen to messages
        client.onMessage(message => {

        })

        // // Listen to state changes
        // client.onStateChange(state => {

        // });

        // // Listen to ack's
        // client.onAck(ack => {

        // });

        // // Listen to live location
        // // chatId: 'phone@c.us'
        // client.onLiveLocation('phone@c.us', (liveLocation) => {

        // });

        // // chatId looks like this: '5518156745634-1516512045@g.us'
        // // Event interface is in here: https://github.com/s2click/venom/blob/master/src/api/model/participant-event.ts
        // client.onParticipantsChanged('5518156745634-1516512045@g.us', (event) => {

        // });

        // // Listen when client has been added to a group
        // client.onAddedToGroup(chatEvent => {

        // });

    };

    async captureScreenshot(sessionName, filename) {
        try {
            const waPage = this.waPages.get(sessionName); // Obtener la página de WhatsApp de la sesión
            if (!waPage) {
                throw new Error(`La sesión ${sessionName} no está disponible`);
            }
            await waPage.screenshot({ path: filename });
            console.log(`Captura de pantalla realizada para la sesión ${sessionName}: ${filename}`);
        } catch (error) {
            console.error(`Error al capturar pantalla para la sesión ${sessionName}:`, error);
            throw error;
        }
    }
    async createSession(sessionName, socketId) {
        try {
            if (this.connected) {
                var valStatus;

                const clientStatus = await this.getSessionVenom(sessionName);
                if (clientStatus) {
                    valStatus = await clientStatus.isConnected();
                }
                if (!valStatus) {
                    const client = await this.startSessionVenon(sessionName, socketId);
                    if (!global.clients) {
                        global.clients = [];
                    }
                    global.clients.push(client);
                }

                return { success: true, message: `Sesion ${sessionName} procesando...` };

            } else {
                console.log('WebSocket no conectado');
                return { success: false, message: `Sesión ${sessionName} ,WebSocket no conectado` };
            }
        } catch (error) {
            console.error('Error al crear la sesión:', error);
            return { success: false, message: `Error al crear la sesión: ,${error.message}` };
        }
    }
    async getSessionVenom(sessionName) {
        const client = global.clients.find(({ session }) => session === sessionName);
        return client || null;
    }
    async sendMessage(sessionName, { to, content }) {
        if (!this.connected) return { success: false, message: `WebSocket no conectado` };

        try {
            const client = await this.getSessionVenom(sessionName);
            if (!client) return { success: false, message: `No se encontró la sesión ${sessionName}` };

            const result = await client.sendText(`${to}@c.us`, content);
            return {
                success: result.to.fromMe,
                message: result.to.fromMe ? `Mensaje enviado a ${to}` : `Error al enviar el mensaje`
            };
        } catch (error) {
            return { success: false, message: `Error: ${error.message}` };
        }
    }



    async getAllContac(sessionName) {
        if (!this.connected)
            return { success: false, message: `WebSocket no conectado` };

        try {
            const client = await this.getSessionVenom(sessionName);
            if (!client)
                return { success: false, message: `No se encontró la sesión ${sessionName}` };

            const users = await client.getAllChats();

            const listArray = users.map(chat => {
                if (chat.isGroup) {
                    return {
                        id: chat.groupMetadata.id || "",
                        isGroup: true,
                        name: chat.groupMetadata.name || "",
                        owner: chat.groupMetadata.owner ? chat.groupMetadata.owner._serialized : "",
                        participants: chat.groupMetadata.participants || [],
                    };
                } else {
                    return {
                        id: chat.contact?.id || "",
                        isGroup: false,
                        name: chat.contact?.name || "",
                        owner: "",
                        img: chat.contact?.profilePicThumbObj?.eurl || "",
                        participants: [],
                    };
                }
            });

            return { success: true, message: listArray };
        } catch (error) {
            return { success: false, message: `Error: ${error.message}` };
        }
    }


    // async sendLocation(sessionName, { to, lat, long }) {
    //     try {

    //         const client = global.clients.find(client => client.session === sessionName);
    //         if (!client) {
    //             return { success: false, message: `No se encontró la sesión ${sessionName}` };
    //         }


    //         const result = await new Promise((resolve, reject) => {
    //             client.sendLocation(to + '@c.us', lat, long, 'Mi ubicacón')
    //                 .then(result => {
    //                     resolve({ success: true, message: result });
    //                 })
    //                 .catch(error => {
    //                     reject({ success: false, message: error.status });
    //                 });
    //         });

    //         return { success: true, message: `Ubicación enviada` };

    //     } catch (error) {
    //         if (error.message.messageSendResult == 'OK') {
    //             return { success: true, message: `Ubicación enviada` };
    //         }
    //         return { success: false, message: `${error}` };
    //     }
    // }

    // async sendMenu(sessionName, { to, menu }) {
    //     try {

    //         const client = global.clients.find(client => client.session === sessionName);
    //         if (!client) {
    //             return { success: false, message: `No se encontró la sesión ${sessionName}` };
    //         }

    //         let sections = [
    //             { title: 'Opciones ', rows: [{ title: 'caldo de gallina', description: 'pierna,huevo,papa' }, { title: 'Estofado' }] }
    //         ];

    //         let list = new List('Lista de opciones', 'Mi menu', sections, 'Que menu desea', 'footer');

    //         const r1 = await client.sendMessage(to, list);

    //         return { success: true, message: r1 };

    //     } catch (error) {
    //         return { success: false, message: `${error}` };
    //     }
    // }
    // async sendImgBase64(sessionName, { to, base64, descripcion }) {
    //     try {
    //         const client = global.clients.find(client => client.session === sessionName);
    //         if (!client) {
    //             return { success: false, message: `No se encontró la sesión ${sessionName}` };
    //         }

    //         let media = new MessageMedia('image/png', base64);

    //         const response = await client.sendMessage(to, media, { caption: descripcion });

    //         if (response.id.fromMe) {
    //             return { success: true, message: `Imagen enviada ..` };
    //         }
    //     } catch (error) {
    //         return { success: false, message: `Error: ${error}` };
    //     }
    // }

    // async sendPdfBase64(sessionName, { to, base64, descripcion }) {
    //     try {
    //         const client = global.clients.find(client => client.session === sessionName);
    //         if (!client) {
    //             return { success: false, message: `No se encontró la sesión ${sessionName}` };
    //         }

    //         let media = await new MessageMedia('application/pdf', base64);
    //         media.filename = `${descripcion}.pdf`;
    //         const response = await client.sendMessage(to, media, { caption: "pdf" });


    //         if (response.id.fromMe) {
    //             return { success: true, message: `Pdf enviada ...` };
    //         }
    //     } catch (error) {
    //         return { success: false, message: `Error: ${error}` };
    //     }
    // }
    // async sendPrueba(sessionName) {
    //     try {

    //         const client = global.clients.find(client => client.session === sessionName);
    //         if (!client) {
    //             return { success: false, message: `No se encontró la sesión ${sessionName}` };
    //         }

    //         var rootPath = path.resolve(__dirname);

    //         rootPath = rootPath + "\\screenshot125.png"




    //         const res1 = await client
    //             .sendImage(
    //                 '51968946598@c.us',
    //                 rootPath,
    //                 'image-name',
    //                 'Caption text'
    //             );
    //         // const res = await new Promise((resolve, reject) => {

    //         //     client
    //         //         .sendImage(
    //         //             '51968946598@c.us',
    //         //             rootPath,
    //         //             'image-name',
    //         //             'Caption text'
    //         //         )
    //         //         .then((result) => {
    //         //             console.log('Result: ', result); //return object success
    //         //         })
    //         //         .catch((erro) => {
    //         //             console.error('Error when sending: ', erro); //return object error
    //         //         });
    //         // });

    //         const base64Image = "image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQgAAAEICAYAAACj9mr/AAAAAXNSR0IArs4c6QAAHUpJREFUeF7t3d2RJMlxBOA5FUA9QAP0lwFmEIQyDO34BGDzjN+N+2XVzvk+R8aPh4dnVG1P9y9/+evfPj9e8O9//vmPH7L4r//+O2V2Ons6ePKXxD3F0Fw0PwLg4+OjXUc77ttxaefX9qf9aNv9MoH4ujBNIFyYnhoYFc52fm1/7cFXfxOIYHOZQEwgfuWAbqY6lLo5q7/EbgIxgUj4w482T92o2yCi9n78IBBJIzWVpxRXyaJ1KFZ6I6g/zS+x0x4ltWmMG3W0YyTb5Q1c9B3YBAJfhLYbnvhLyKxnlaQTiPMjRtJfxV57qXanuBOICcSRP0rSCcQEQgWI7ZR86rBNUo2rjwRJfppL2057lNSmMZLanoqxDSLoWrtpbZJqaROI7PZs80Cfq7VvyoOEf0/lrHHpEUMBUNVsE0Pz05eUaqcEUn9KXK1X+6F1qJ32N7G7UZvmp8OW9Fc5pD1SfxOIwzsIBa/djIRASS56Vu10sBK7CYRvb4rVl19Stm8sJUZCyASUCYQif7bT/iZ22t+kEs1vG0T5TX8CvDZDCaS5vEkk27kkQ6T9UNFVO+1vUptyI8EgOdvmwTYI/NRkQlIlbpt8Ogj6GKP+2gRPsG/X1u6R5qdxJxAH9ikoSTN0OBIyJ0KiZ7UOHXL1l+CifWvnrLWpnWLQtmvn99O9pJxAnCmQDFZ72JT0baFTbugQJXaKQdtOc9a4E4gL71faTWsP1gRCO+R2OoBtO81Q404gJhBHTiW3sZKvLXRJzjpYaqcYtO3a+U0gJhATCJ2q32HXHnz1pymqv28rEApUYqfP/clbaY3RfkxQXJRob98WFGfdUtSf9i3BWbH/Fv/NqQ1Sgid2SoIJRP/FapsHSS91AJVrCV80hgrONghF9GCXkEobpDH0JgrKPR7VOm4MUVKb4qzCpP60bwnOiv02iIRBE4gJxG98/6QOoNJvGwT+gZSqqwLfttNbImm4xngKq+Rme1Ntmss2iMPHkduDpQOjcZOm6Vm9Jdq1tQdfB0HjJv40xlPYay/VLuFzG2fFnt5BaGFqdwPQhFRah96e7ea2RU3rbfdNSZr0MsFe61W7N+Gs2E8gLnwOIiGpNlLJl+TSHoSkthvirPWqnfao7S+JO4GYQCh/6j8QM4E4Qz+BCF5S3iCVTsyNWyypV1dzrfcGcfXx6Qb2Wq/avQln5dVP98taCTG0kbPLflJv+PXxU3Fp200gyl8iMwHz36qckLiQtAdf/U0gJhD8bmED7QPdvih0oNt2E4gJxATi47nBVyFpD776m0BMICYQE4jf1ItfPj8/P1VNbtslb7T1LW37Tb9ipOu61qE3UftzEFqv2iX5aYwb2Cd90zpu2E0gDih/F5Img6AEb5P0u2Cv+N2oN+nRBGICwfzRjY4dfmPsJxAJC/CsElJVuO0PyziaJbe7nlU7rUPxU3/f+fFuApGwAM8qIScQ2Vt4bMfxZaaeVTvtpfprD2oiusnZpN7kLP0vhr4A0xshAUpFQ3NRAiUgJ2cVe7VLckkwTeImPde4ip8KWJvjGlfrVd5PIA5I3SCkNjIh7o06niKu4qd2Cc46bIpVIi5ar+Y8gZhAJJz6UNInQZ4SumRQnzrbxnkCMYFIODWB+A30JhD4PQo3VjdluN52N26sJGcl3406FFOtV1ffxJ++S1GcNWfFKomb4HKKS5+DSIimoNwAOSGGntUGJZhqDBXnn9Gf8kpxbvtTTBO7pL96dgKBf4sxgfD/SlXSK0kT7CcQ524o9hOICYTO8/F9gw6gDrn6a9/4bX8MamCoQ55gP4GYQDBFE0ImJNWz+ph6wx+DGhgm/dCzE4gJBFNUSaUOE3/tG7/tTzFI7Nr4HV9S/uWvf/u3P/dOgGonrOC119L2TaRvpbWOGzeg8kB79CZMb/A06bnmd6NHX/7a+wSAhOBtoinBdXiT5mqMBD+NcYN8SS4J/7RHbZw1bmKnfFa7CQQi9RSZMT3+wFJSh+aidkkuE4jz/yop9mo3gUCkniIzpjeB+AO+Nm4bxMfHBAIncAKBQP0Os6cw1RV+AnEQCO3vjWfUp943aFzFIBkE7cdTOesQJRjo44Ri0M5Z/SX5aYwEq1MM+mMtTS4hs55NiKYxtJETiDOi7Rs6IX3CFz2bzEc7RoLVBCJQiAR4JYEKjpZxI2cdjgSDG3UkoqYY6MWj/dWcE15tg8BuPEVSTO9odiNnHY4JxLmTikuC8wTigF4Ciiq9xlASqD8VjQnE+XdCk2FT7LWXyo0kZ82FHjHUWZt8ybqkICe1JcRIsErEKombYKpxkxgJLtpLrUP9te1u5PeH/zdn0kgF4AbRtLmJ0GmMBJe2SCb13uib1pvwVPvWtlMeJHEnEAf0lLhPrX1KDLXT4WjXqzjrkCf1KgaaSzKUerZd7x4xDn+5qcTQpiU3qsZQYqhdgkFS7wRCO362S/qrkbdBbIM4ciUZXiVuEkNFLbnxtQ4dtrbdjfxIIG4kog1/as19U9x2Lkpc5YEOvtah+andU3UoLslWlszRlx8xFFBtkNrdAFRjKJkVqyRuO5ekH0pmjZHc+BrjRo8SXPSsYpVwbRsEfn1/eyiTprVzeftgaX5qN4FQpPCvORVQD2uWOkSJ4mqM9lAmcdu5WDfOHzpS7DWG3orqT1fuG3VozzUXxUrj7hEj+P7J9lAmTWvnosOmF0VSm5Jec55A+KdJvywQCnLSNCXGm0iquSgu6k/tNK7a6eC3b8Ab/EtEV/FL7BTTJMYEAtFrixWG/dDBVzuNq3YTiOw2VpzfJFb0kvKGgreHUsmszVAMtI7E3wTizqA+dWsrJxOOq1hNIA5I6ZC3B1X9qZ2SQO2UkDpYirOKqdahdlqH+kvsnsplAjGBYN5OIO5sLt9ig1BV11siuRXbZ3li0FDVX7HCsMd3GnpWSao8SOJqLm0eaN+S/PTsDZxP9X55g9CElfQ3mqs5v53Mmp/e+Oov6aXGUDsd3iRnjaFDrv1Iclb8NOcJRPBJSm2GEk2JoXGVkOpP82vHVTLfuGS0Nu251nbjctsGcUBZG65DpA1PyKy5tGubQPg7iAkEDltCqqfO6gCqnZJF69W4Ewj/eToVbMVUe64Xyqs3iAQULUwBTeySwUqGV/FLyJLg3D6r/m7Uqz1P+qv13ojRxpTeQSjBFYBErScQSnn/46obBG9zSHNWtJS76k85rv6SepPaJhDlR6WkkW31T0ianE0wSMisItTGWetNatMY7domEBOI4yU2gUju9mx708gqiIkwTSAmEBOID3+ZmQxvMqiv2SAUAF1l9CZSNUzy07PayHbOGlfrSOy0Nn0nlPDlZzyrvVSctZcaVwXny7/NmTQtIZUCldgpyG9qblKvkiXp+Z/t7M/IoROfJxAH5v6MzZ1APPchJt2SEyHW/ip3NZcJxASC30H82baApF4d1DdtodsgUIZ/xuZiaWymxE0eF7/z2Z+RQ0eB+Pz8/GTW/D+GumqpXSuv3+tH80uGSHPSGOpPiasr6I1bVmtL7BRnFTW1u4FfwudfJhA/tigBVBuuZFbiqr8JxBkpxVkHX+2UL8pJFXbNbwJxQFSbkZBKB1pjqL8JxATiVwQmEDoxEwhGSsVKyZeIFSeNhjdquxFjGwQ2PDHbBvHcLZv0LTl7Y3hvxKgLxF/++rd/e0n5lPonQ9m+iTQXbUbynLmz/jHo9gAq9ipM7fyU9xr3VMeX/xZDk0vAe7tYTSCe+3DSm7BPOK4idOPSmkBgJ280Q8UvIdB3PjuB6G9WE4gJBL+9fru4TCAmEEzmpx533kRSfc78LpvLm7DHe4d/p0R7pLxXbnx5g0gA0GI1xg07BT4hqdbxJvwSXLRetVPSJzknj5ptbigP1E43RHpJmTQtSVjjtu3apEryexN+CS4JBsmwJTlPID4+JhAH9rVJlQzHBOKM3jYI/1q7BKsJxASC9SsRTg6ChgnpMcTxnUGCgeas679uOBr3VNsEYgKh83J8acyHy4YJ6TUVHcDEn57VTVLtVIToj7UUqBtNU0BvPLcqLppz4k+xV2Ik+GkMrbddWxI3GUDlQRu/pJcTiGCDUKIpMRJ/7SFKSNUmeLs2xTmx056rnQqTPgIpphOICQQ/OiipJhA69m43gXCsvmypBE9UWM/qDa3+tDYdXs1Pm5EQvF1bshloHYqL2mncNl+2QWyD2AbxO75ARQdVB1/tNO4jAqG3SZKcAqA34JtuHSWB1pb4U1y0l21uqD/Fqr0tKPaKs/rTfmhc9UcbhDZNgyZNS4iRNKOdc5KLntW+KaYaV7FK/GnOmovatXNWf8lsKVYnuwkEPmIogVTB28RQf5qfElJFqO1PSZ/07UbO2jfNpd3fCcQE4shRJeQE4jziOqgTiAMCiaonN0fSjHbOSS56VodXMdW4ilXiT3PWXNSunbP6U8FWYVJ/9NN7+gJRgyoo7aa14yb56dnETutN+tbOT3PRuIpBIqYqVppLMuRtXCYQ5Q2nTXBtuJLqBpn1QklyUVx0KCcQZ6QmEBMI/hyEDtEEwr8OTjFVMW0L5wRiAjGB+I01401b2QQiGNRkjVQF11txjxj+s25K+qRHCTcmEIdvlNKmJeAlTdOzNwY6wSrJT9fIGz1SQdQhv9HfBD/tm9bxdrvqI8abitVGKsF12JK4PyNxFb8JxJumw3OZQPzzH4TWBOIM0wTCf12MiPYyownEBCKi5ARiAsE/YBMxrXw4WfWTdTiJu0cMJ8GfDWdHpmv55S+tTVZuLSGJoWc1F7V7O3HfdOMrpmrX7rn2sv2y+ka9WtsEQruBdgr8m+ywNP7pOB0Yjat2EwhFyv8regLhmJLlmwZfH1mosI/sWTvZXG7kp6KmIpTw4Ea9mt8EQruBdgr8m+ywtG0Qv0Mkk/4+1Y+T+E0gtBtolxBDb/y2HZY2gfgzCsTn5+fnvxJEVygl1Y2BOeWSDFFS21O5JDnfwEpXeK2jjfMNniY4t/NTnH/4RqkJhELnf7GXEMOzMcuniDaByH5s96m+TSD+++82WQcrffE2gThDrPhpgxKc2wP4dn+K6QRiAsG/Yn1ju1Ti7hEj20gU5wnEBGIC8Qf8cM632SD+8te//dtLyhuFqXrpjZWsqhpDn6GTNVdx0R5pzhpXa0tu96dyaWP1Jgy0b1/+b84kwI3hvRFDCZRgpcMxgfD1WgdV+6s90rht7io31I4+B5GQvg1Au5HbIJzyyoM3DYfm0uaVxm3Phw6+2k0gyu8gdIhuCFOb9Frbm4ZDc2ljpXEnEPh9Cwpou5E3BlXVWu/yxN8NQmov35RLm1dvwkCF/fgO4j8/SamFKZm/i12bzIqLxk2ETnNJ7G6ImmKgmH6XWVDsT3b025wKfEKgt599ilQa9+09UpImN7lioJhOID4+JhCoTE+RSuPqcGC5dbMJRB1SdqjYb4NgSH801EFt3zoadwLh31ehmLZ7GdAvOjqBiOCzw0+RSuNOICYQv8XkSCD+85OUNi5nqyQRVetkYJL8dAA1RvtZO8ElyUX7pjEU58RfglXSX81ZZzD53wnFgL72XhNW8DQ5BUBJmuSnxNUYCVnauCS5KPYaQ3FO/LX5185Z5015oHanuBOIwwel2kOuBGoTtz28CXGTgda42rc2ztpfxUDr1cFXuwnE4UNbCp6SQEmakEVznkD0/2Yj6W/Sc+2l5qciuQ1iG8TxwlICKXF1OFSIE39amwpxO+efboNQVdLCtLlKPm24+tP8lBg38FPsE6wUP42hA6i1Jf1IYiT91bNqp3WoP9og1JkmpwPYJqT60/wSQurZBFOtI4mRDHlytt1LxUBz1v7qbKmd1qH+JhD415wKqA6lEkgbrnZ6u6s/HRgd6CQ/xbQd4wY3khgJJycQEwjVgqPdBMJ/xi4Z1AkE/ll4Qki9xZJGts9G03s4nNyeip/GeFMvFWfNOdlm2jESTv7hX1qrySXkU0ATEmgdSoy2Px3KBGc9q7VpPzSu3rJql9ShnFS7p7CaQByQ1yFPiKbkS3JRUiUkvZHfBEI7ebbT/p7sJhATCP7aex1UFb+E9gnpE2HXTa2d31NYTSAmEBOI8Ed5VThVmFSEVDQSsZpATCAmEBOI/5sCesS4oUq6gurzrebcVvBEmRWD5HZqn038JVhp3xJM27UlnNSzapdgT185dwO8hAQKlMbQFS8BPiGzxk36pphqLjfsEkwTrJKLTLmm/WjXMYHAP9ZqA5+QWYctyVkJqbncsEswTbCaQOBzut7Q2sgE+CSGqnpCes0vIW77bOIvwarNqxvipzGUa+qv3aNtENsgPhKS3hj8CUQiD/6dGPSSMrm120TTWzbJWaFvk1RzVkzb/hSXdtzEn57V2hI77VsSI6lX+Vz9b84EFC1WC0uAbwtTkrNimuCXYNWOm/jTs0m9elb7pv700UH9KScnEIioAnpDXBKytImrQ6lxE396FlsemWm9SZCkXuXzBAI7pIBOIM6A6sAo6ZN+YMsjM603CaJYJZycQGCHEkK2z26DyL6DAVsemf3pBUJJr3ZJN240Q/Nrq3pbDDQ/xTTxl/wPiN6KSd8S7ip+ml+bBxr3yxuEgqd2mnCbGEncpGlJHe2hfFMuCV+SoXyTWCknEx5ojAmEIoV22rQ3DeWbcplAINH+gD8wO0WeQHg/yHICcYZJB1/tElHTze+pXIhoEwiF6fzCyk93LScQE4hfEUged5SRyrUklx++1VpVU+1uKH07RtIgbcaN5upNqfWq3Zu4of3Q2hTTBIOEz/ouReudQChSB7ukGRMI3zS0RUk/NMYE4qEfuNUGPTVYCTH0bHJztPFTf5rzjb5NIPwPs7S/2yAUqW0QjFSyXiePBBOICcTxe/P0FmOGo2FCyBs3arK5IARHswlE/5OeKpwJJ0/N/PIGoQRSsiQAJLnoECV1tJvWFpfE342zegHcwFl50M5Za0vsJhDln/dLmqGCqAKmhGz7m0Cc13rtR5tDib8JxATiuGzdGPLk5m0Pm26c7WFTDJ6ym0BMICYQqg6/8UlFHd62qCVileS8dxDB/060m6bcTW78PWIoyv4/Akk/2hxK/FU3CFUlfdbWwpTgb8/Padq1TPrRzcS9JTknPFBOql2bu4qgYjCBuPAN1koCbW7bLhm2di7qL8lZh+OGnXJDc0nw063ny48YWkS7uW2Qn8pPm9u2S+pt56L+kpwTnupmoHZt7ib4TSCCj4wnjdSz2ty2XTJs7VzUX5LzBCL70NY2iPL/bEwgdOzdbgLhn7U4oaoi+eV3EN7KHy11ldEYbbLoQCcga85JLorfU/3Q2rSOG5gqVsoNzflNcWmD0Ka1SaBqqHGTBikJkpzbdai/pL+KaTuXdtx2f9vvJZRX7bgTiAPyCnKi9DqUmkviT88qSdWf4ncj7gTi/BgzgZhA6Dwf7do3uSbTjjuBmEDw9wTqra034A0y62BpzuqvXdtTcScQKBA3GtR+Hv3OOWttuoa3xa+dX8INHXIVNRXTdlzFQONqj07+fnjESJzpWQUg8acDk8RQAiUxkrNKILVLckn6oThrHROIcycnEPiZBx0EJa76a9slA3OjtrcPqmKQ4Kw9v7H5TSAmEMev7FOCK5nVbgKhSPX/slQ3uj1ieI9+sNQbJggRHdXBV7somcPhCYQj+tNtEFraDfIp0fTdh+asdqrWimm7Ds3vBkmTXBL8Eg5pXMVP/amdxj3ZfXmD0OSSIUpi6NkEvKS2NiGTOpKhTDDQHj0VQ/NTO+2R+lM7jTuBOCCagJcQdwKh9M7+GlGjtPuRbHmas9olHN8GgV8ik4CsN7Q2PCGfvjdR8VO7pLanYiQ5Jz16Ku42iG0Q0Q8PtUVSB2ECoUid7ZK+/eEbhN6eWkQG1ddPa35qp5m0/enNlvRNa0vs9JFA8buxWWm9mov6U4Gtfg5Ck0uIpiRIctGzCdGSOjSu1jGByN5pJMOmPZpAlL8OToFP7HRQ1U5zafubQEwgfuWA8mqPGDipCqjaYVhupPqbQEwgJhDJtPzGWR18tdMU2/4mEBOIukDoc5eS/oadPvfrAH5nO+2HYqoi1OaV+lM7fX+m+KldwjWNoXb0iJEAqom07ZTMSTMUl7fbKfaK6QRCET3bJZzMIv94egJx4YNSEwh/KZa8wW/jvA3i42MCMYHgS2cbxPn9BQOIhtsgEKjETMmcNKN9Yz3lT3FWTPeIoYj+BI8Yn5+fn/+apq54CVluEEiHTdfIp3BRrLSOG/40hvYo4VrSNz2r2LcvI8U5katfJhA/wqfEVWIkDUpIoIOlg6D+NGfFuR1X+6a4JP7aGCQ5n+qYQBxQ0aYpMSYQ2UvKCYS/+5hAHF4q6qAqeBOIs6S1B1Vxbsdt8yXx18ZAOa6X1jaIbRDRn4Ar0W48f+ujTTLQWm8y+IpVUq/W8YNA6MF2EcktoTkngGrDNRf117bT/NRO+9bmi+andm/HuV2HiuQE4sIjizZDhyixU6Kp3QTi/H5F8WvbqdApJycQE4iIoxOICcSRQHqL6VqvRIvYfDisL3USZVa1VkwTuzZ+2jfNWflyow7NWTnUzjnhlZ7dBrENIuLtBGIbRESg5PANBU8IriqsGGi9b/KnudywS7Y8Pat2Wq/yTzcrzU/jfnmDUAASOx2YZMVToDSG+ksarpgqfok/PXvDTodDhV3xU25oXMUqyU95OoHAH/RVEijwEwgdA7ebQPi3ZSlPJxATCJ5AFUl2WDacQEwgPhISJCueDocq8zaIsjp8+HAoD5IVXqu7wRet45QzfWGMFpvYKVBarNrpoCqpFAPNT4VJ46rAJnaKqfZca2vH1R5pfuovsWvzdAJxQFSHMiF4mwRK0mTwNef2oGpt7bhJvUkuGvcGTycQEwj+7Q0lbjIciRi04yb1Jrlo3AkEDm9yK2oj26tbmwQ6WAlWmrNimmxgWu+bctZcErs2T7dBoAi1gW+TQAdmApF9+UoiaknPtW9tnv4gEAkASlIF6il/GldvysSfEiOx0/x0pVWSJv40Z7VLeK98VjvN+Ya/CYR2A+3apE8G/waBEJb6f09rXLWbQJz/pmQCoQxCuwnEGSgVOoS5bjaBmEDUSbVHDId0AuFf3Kuo3tgQt0FoN9BuG8Q2iF8RUEG8sbkgdY//3U0CkZA+ASpRyCRnBbRtp1i14yb+NOfETvNL+KLbYHugtbbETnM+4TeBSJAvn9UhKoeN3GnOiZ0mOIHw7U0FcQKh7Ltgp0N0IRUOoTkndprMBGICcXy2UzVUoj1lp0P0VH6nuJpzYqf1TiAmEBMInZZLdsng/4wDrc/zb7q0NOc/1TuIZD4S4rbPah06qOrvhp3m3LbTTei7DLn28oTzt30HoaAoCRIVTs5qHTpE6u+GnebctptA+KPIBOKAVXsLmEA4IRX7xG4C4f2YQEwgbiwLxxjtzUD9TSAmEBHp9Xa68XiihSTDoTHadppz224C8U0EQgmpn5pMiKZioDnrY4f60/wUg2SINJd2jHZcvSjUTnup/tRO457sXv2IoYVNIM5IKYES/G70SGNMIPzLcBTTCcThtzn1dtfBunFT6nBsg/D1OhFY5VC7b0ncbRCHH8lREmgjVZnbjdT8JhATCOXor3bbILZBHP/Mt7316LbVFs4krl4eaqeDqf7UTuN+eYNIAvyMN5uSVBukJNWh1LiKvcZNeJDknMRVDLTnbX83uKH9/fJHrZ9qkIL3puYmK3zSSMXgBqbtIXqKfxo3ET/tR8KN5Cw9YihQaqeAKng6HG3itutIGqkY3MC0jbPySu20bzf8aT8SbiRnJxDldxDbIM5j1R5KHd4bYpXUNoE4dEgBVfD09myTpV1HovSKwQ1M2zgnYnAjF+WB5qL1JpeRnn31L2sp8DfsdHiVBMlAt88qfkpc9ad27bjq7029fCrnCQR+NmIC4RTVwVc7jdz2N4E4fA5Cm9G205XnKbsJhHdcB1XtNHLb3wRiAsG/XzCB0DH1H4hpD3Tb3wRiAjGB+PCBVonQQVW7dlz1N4H4+PhftyTqXCtW3ksAAAAASUVORK5CYII=";

    //         const res2 = await new Promise((resolve, reject) => {

    //             client
    //                 .sendImageFromBase64(
    //                     '51968946598@c.us',
    //                     base64Image,
    //                     'image-name'
    //                 )
    //                 .then((result) => {
    //                     console.log('Result BASE64 : ', result); //return object success
    //                 })
    //                 .catch((erro) => {
    //                     console.error('Error when sending: ', erro); //return object error
    //                 });
    //         });

    //         return { success: true, message: `Menu enviado` };

    //     } catch (error) {
    //         return { success: false, message: `${error}` };
    //     }
    // }
}

module.exports = VenomRepository;

