
require('dotenv').config();
const express = require('express');
const sessionRoutes = require('./controllers/SessionController');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(express.json());

app.use('/session', sessionRoutes);
app.use('/message', sessionRoutes);
app.use('/sendImgBase64',sessionRoutes);
app.use('/sendPdfBase64',sessionRoutes);
app.use('/getallcontact',sessionRoutes);
app.use('/sendLocation',sessionRoutes);
app.use('/sendMenu',sessionRoutes);
app.use('/prueba',sessionRoutes);
//captureScreenshot
app.listen(process.env.PORT, () => {
    console.log(`Server listening on http://localhost:${process.env.PORT}`);
});
